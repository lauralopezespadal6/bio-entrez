import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Entrez',
  description: 'xtec.dev bio app',
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html>
      <head>
        <title>Entrez</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href=" https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" />
      </head>
      <body>{children}</body>
    </html>
  )
}
